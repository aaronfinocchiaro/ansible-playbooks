#!/usr/bin/env python3
import getpass
from solarwindslib import *


"""
Written by: Aaron Finocchiaro

This is a script that makes a query against solarwinds and dumps the results into an Ansible host file
along with the standard variables that are required.

"""
#get credentials from user for solarwinds
def get_creds():
  username = input("Solarwinds User:")
  password = getpass.getpass()
  global sw_username
  global sw_password
  sw_username = username
  sw_password = password

#query solarwinds for desired hosts
def solarwinds_query():
  listofhosts = solarwindsquery(f"SELECT DISTINCT ns.caption FROM NCM.NodeProperties n INNER JOIN Orion.Nodes ns ON n.CoreNodeID=ns.NodeID LEFT JOIN NCM.EntityPhysical P  ON p.NodeID=n.NodeID AND p.EntityClass=3 WHERE ( MachineType LIKE 'Cisco' AND p.model LIKE 'C9200L%' and status = 1 )", sw_username, sw_password)
  return listofhosts

def main():
  get_creds()
  host_list = solarwinds_query()

  #open host file to write all of this to
  with open("ansible_hosts.txt", 'w') as host_file, open('host_template.txt', 'r') as var_template:
    host_file.write('[allhosts]\n')
    for host in host_list:
      host_file.write(host['caption'] + '\n')
    host_file.write('\n' * 2)
    for var in var_template.readlines():
      host_file.write(var)


if __name__ == "__main__":
  main()
