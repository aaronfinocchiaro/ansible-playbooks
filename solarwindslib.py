#!/usr/bin/env python3
import requests
import json
###############################################################################
#By Joel McGuire UNIVERSITY OF ARIZONA
#
#
#This is python3 will likely not work if python2
#
#Summary:Defines a function that takes as an argument a swql query and returns a json reply
def solarwindsquery(query, username=None, password=None):

    if not username or not password:
        username = 'joelmcguire' # Your solarwinds username.
        filename = 'Creds.txt'   # This should be in .gitignore.
        file = open(filename, "r")
        # This should be adjusted for the length of your password
        password = file.read(21) 
        file.close()

        #This is to get past the SSL error since there is no cert on solarwinds
    verify = False   
    if not verify:
       from requests.packages.urllib3.exceptions import InsecureRequestWarning
       requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
    url = ('https://solarwinds.arizona.edu:17778'
              '/SolarWinds/InformationService/v3/Json/Query')
        #Building the SWQL Query. This will need to be adjusted. 
        #If you download the SDK it comes with a simple query builder you can
        ####use to test your queries called SWQL Studio
    payload = {
        "query": "{}"
        .format(query)}
        #Note that this is a post request.
    r = requests.post(url, verify = False, auth=(username, password), json=payload) 
    #return r.json()
    test = r.text[11:-1]
    jsonobject = json.loads(test)
    return jsonobject
